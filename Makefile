
prefix = /usr
sbindir = $(prefix)/sbin
sysconfdir = /etc
fwconfdir = $(sysconfdir)/firewall
sharedir = $(prefix)/share/firewall
INSTALL = install
TABLES = filter nat mangle raw
CONF_DIST_DIRS = $(TABLES:%=%.d) reload-hooks

all: update-firewall

clean:
	-rm -f update-firewall

install: all
	$(INSTALL) -d $(DESTDIR)$(sbindir)
	$(INSTALL) -d $(DESTDIR)$(sharedir)
	$(INSTALL) -d $(DESTDIR)$(fwconfdir)
	$(INSTALL) -m 755 update-firewall $(DESTDIR)$(sbindir)/update-firewall
	$(INSTALL) -m 755 update-ipset $(DESTDIR)$(sbindir)/update-ipset
	(for d in $(CONF_DIST_DIRS); do \
	  $(INSTALL) -d $(DESTDIR)$(fwconfdir)/$$d ; \
	  $(INSTALL) -d $(DESTDIR)$(sharedir)/$$d ; \
	  if [ -d conf-dist/$$d ]; then \
	   for f in conf-dist/$$d/* ; do \
	    $(INSTALL) -m 644 $$f $(DESTDIR)$(sharedir)/$$d ; \
	    b=$$(basename $$f) ; \
	    ln -s $(sharedir)/$$d/$$b $(DESTDIR)$(fwconfdir)/$$d/$$b ; \
	   done ; \
	  fi ; \
	done)
	chmod a+x $(DESTDIR)$(sharedir)/reload-hooks/*
	(for type in ip net ; do \
	  $(INSTALL) -d $(DESTDIR)$(fwconfdir)/blocked/$$type ; \
	  for proto in ipv4 ipv6 ; do \
	    $(INSTALL) -d $(DESTDIR)$(fwconfdir)/blocked/$$type/$$proto ; \
	  done ; \
	done)

%: %.in
	sed -e s,@PREFIX@,$(prefix),g -e s,@FWCONFDIR@,$(fwconfdir),g $< > $@
