# Set up basic rules for the 'filter' table.
#
# This snippet should run before the others.

# Set up a chain that will drop noisy unwanted traffic
# without even logging it.
create_chain drop-noise
add_rule -A drop-noise -p tcp --dport 113 -j REJECT
add_rule -A drop-noise -p tcp -m multiport --dports 139,445 -j DROP
add_rule -A drop-noise -p udp -m multiport --dports 137,138,500 -j DROP
# Be kind and allow old-style traceroutes.
add_rule -A drop-noise -p udp --dport 33434:33500 -j REJECT

# base-input chain.
create_chain base-input

# Enable everything from localhost.
add_rule -A base-input -i lo -j ACCEPT

# Some IPv6-specific ICMP setup.
add_rule6 -A base-input -m rt --rt-type 0 --rt-segsleft 0 -j DROP 
for icmp6type in 133 134 135 136 ; do
    add_rule6 -A base-input -p ipv6-icmp -m icmp6 \
              --icmpv6-type ${icmp6type} -m hl --hl-eq 255 -j ACCEPT 
done

# Standard conntrack stuff.
add_rule -A base-input -m conntrack --ctstate RELATED,ESTABLISHED,UNTRACKED -j ACCEPT
add_rule6 -A base-input -s fe80::/10 -p ipv6-icmp -m icmp6 \
          --icmpv6-type 129 -j ACCEPT

# Enable 6to4 protocol.
#add_rule4 -A base-input -p ipv6 -j ACCEPT

# Allow useful ICMPs (but rate-limit incoming echo requests).
for icmptype in 3 4 11 12 ; do
    add_rule4 -A base-input -p icmp -m icmp \
              --icmp-type ${icmptype} -j ACCEPT
done
for icmp6type in 1 2 3 4 128 ; do
    add_rule6 -A base-input -p ipv6-icmp -m icmp6 \
              --icmpv6-type ${icmp6type} -j ACCEPT
done
add_rule4 -A base-input -p icmp -m icmp --icmp-type 8 \
          -m limit --limit 3/s -j ACCEPT
add_rule6 -A base-input -p ipv6-icmp -m icmp6 --icmpv6-type 128 \
          -m limit --limit 3/s -j ACCEPT

# DHCPv6.
add_rule6 -A base-input -s fe80::/10 -d fe80::/10 -p udp -m udp \
    --sport 547 --dport 546 -j ACCEPT

# This must go after the ICMP v6 matches.
add_rule -A base-input -m conntrack --ctstate INVALID -j DROP

# pre-input (runs before conntrack etc)
create_chain pre-input

# user-input
create_chain user-input

# Always allow SSH access, just in case someone forgets to add it
# with a user-defined ruleset file.
allow_port tcp 22

# Setup the INPUT chain.
# It is split into stages: pre/base-input, user-input
add_rule -A INPUT -j pre-input
add_rule -A INPUT -j base-input
add_rule -A INPUT -j drop-noise
add_rule -A INPUT -j user-input
